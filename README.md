CONTENTS OF THIS FILE
---------------------

* Introduction

This module allows you to set a limit to how many nodes of a certain type
can be marked as "sticky at top of lists"

* Configuration

Go to "Structure > Content types > [my content type] > edit"

Go to "Publishing options" and you will see an option
"Limit the number of nodes that can be sticky at top of lists"

Give it a number or set it to zero for no limit

* Maintainers

Rob Duplock https://www.drupal.org/u/robertduplock
